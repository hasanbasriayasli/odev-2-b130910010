﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HaberCom.Startup))]
namespace HaberCom
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
